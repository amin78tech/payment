<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPayments extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function paymentDetails()
    {
        return $this->hasOne(PaymentDetails::class);
    }
}
