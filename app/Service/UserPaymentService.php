<?php

namespace App\Service;

use App\Repository\UserPaymentRepository;
use Illuminate\Support\Carbon;

class UserPaymentService extends BaseService
{
    public function __construct(UserPaymentRepository $repository)
    {
        parent::__construct($repository);
    }

    public function findTopGateway()
    {
        return $this->repository->findTopGateway();
    }
}
