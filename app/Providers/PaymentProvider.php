<?php

namespace App\Providers;

use App\payment\gateway\ZarinPalGateway;
use App\payment\gateway\ZibalGateway;
use Illuminate\Support\ServiceProvider;

class PaymentProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind('zarinPal', ZarinPalGateway::class);
        $this->app->bind('zibal', ZibalGateway::class);

    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
