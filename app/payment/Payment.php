<?php

namespace App\payment;

use Illuminate\Http\Request;

class Payment
{
    public function __construct(public PaymentInterface $payment)
    {
    }

    public function run(array $request)
    {
        return $this->payment->send($request);
    }

    public function callback(Request $request)
    {
        return $this->payment->callback($request);
    }
}
