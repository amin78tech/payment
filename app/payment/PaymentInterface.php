<?php

namespace App\payment;

use Illuminate\Http\Request;

interface PaymentInterface
{
    public function send(array $data);

    public function callback(Request $request);
}
