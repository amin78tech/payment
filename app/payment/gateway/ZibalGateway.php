<?php

namespace App\payment\gateway;

use App\Enum\GatewayEnum;
use App\Enum\StatusPaymentEnum;
use App\Exceptions\CancelOrFailPaymentException;
use App\Exceptions\PaymentProblemException;
use App\payment\PaymentInterface;
use App\Repository\UserPaymentRepository;
use App\Trait\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class ZibalGateway extends UserPaymentRepository implements PaymentInterface
{
    use ResponseTrait;

    protected array $route = [
        'routeRequest' =>   'https://gateway.zibal.ir/v1/request',
        'routeGateway' => 'https://gateway.zibal.ir/start/',
        'routeVerify' => 'https://gateway.zibal.ir/v1/verify',
    ];
    protected array $paymentSend = [
        'merchant' => 'zibal',
        'callbackUrl' => 'http://127.0.0.1:8000/callback/zibal'
    ];

    public function send(array $request)
    {
        $this->paymentBody($request);
        $request = $this->requestBodySend($request);
        try {
            $response = Http::post($this->route['routeRequest'], $this->paymentSend);
            $request['authority'] = $response->json('trackId');
            $this->create($request);
            return
                $this->responseJson(['link' => $this->createRoute($response->json('trackId'))],
                    Response::HTTP_CREATED);
        }catch (\Exception $e){
            throw new PaymentProblemException();
        }
    }

    public function callback(Request $request)
    {
        $this->validate($request);
        $this->findByAuthority($request->get('trackId'));
        if ($request->get('status') == '3'){
            $this->update($request->get('trackId'), ['status' => StatusPaymentEnum::NOK->value]);
            throw new CancelOrFailPaymentException();
        }
        DB::beginTransaction();
        try {
            $payment['trackId'] = $request->get('trackId');
            $payment['merchant'] = $this->paymentSend['merchant'];
            $response = Http::post($this->route['routeVerify'], $payment);
            $data = $this->updateWithInsertDetails($request->get('trackId'),
                ['status' => StatusPaymentEnum::OK->value]
            ,   ['status' => $response->json('status')]
            );
            DB::commit();
            return $this->responseJson($data, Response::HTTP_CREATED);
        }catch (\Exception $exception){
            DB::rollBack();
            throw new PaymentProblemException();
        }
    }

    public function paymentBody(array $request)
    {
        $this->paymentSend['amount'] = $request['amount'];
        $this->paymentSend['description'] = $request['description'];
        $this->paymentSend['gateway'] = GatewayEnum::ZIBAL->value;
    }
    public function requestBodySend(array $request)
    {
        $request['gateway'] = GatewayEnum::ZIBAL->value;
        return $request;
    }

    public function validate(Request $request)
    {
        $request->validate([
            'trackId' => 'required|exists:user_payments,authority',
            'success' => 'required'
        ]);
    }
    public function createRoute($authority)
    {
        return
            $this->route['routeGateway'] . $authority;
    }
}
