<?php

namespace App\payment\gateway;

use App\Enum\GatewayEnum;
use App\Enum\StatusPaymentEnum;
use App\Exceptions\CancelOrFailPaymentException;
use App\Exceptions\PaymentProblemException;
use App\payment\PaymentInterface;
use App\Repository\UserPaymentRepository;
use App\Trait\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class ZarinPalGateway extends UserPaymentRepository implements PaymentInterface
{
    use ResponseTrait;
    protected array $route = [
        'routeRequest' =>   'https://api.zarinpal.com/pg/v4/payment/request.json',
        'routeGateway' => 'https://www.zarinpal.com/pg/StartPay/',
        'routeVerify' => 'https://sandbox.zarinpal.com/pg/v4/payment/verify.json',
    ];

    protected array $paymentSend = [
        'merchant_id' => 'd4254174-4e9f-4e05-815e-07078af940e5',
        'callback_url' => 'http://127.0.0.1:8000/callback/zarinPal'
    ];


    public function send(array $request): string
    {
        $this->paymentBodySend($request);
        $request = $this->requestBodySend($request);
        try {
            $response = Http::post($this->route['routeRequest'], $this->paymentSend);
            $request['authority'] = $response->json('data.authority');
            $this->create($request);
            return
                $this->responseJson(['link' => $this->createRoute($response->json('data.authority'))]
                    ,Response::HTTP_CREATED);
        }catch (\Exception $e){
            throw new PaymentProblemException();
        }
    }

    public function callback(Request $request)
    {
        $this->validate($request);
        $this->findByAuthority($request->get('Authority'));
        if ($request->get('Status') == 'NOK'){
            $this->update($request->get('Authority'), ['status' => StatusPaymentEnum::NOK->value]);
            throw new CancelOrFailPaymentException();
        }
        DB::beginTransaction();
        try {
            $payment = $this->findByAuthorityVerify($request->get('Authority'), $this->paymentSend['merchant_id']);
            $response = Http::post($this->route['routeVerify'], $payment);
            $this->update($request->get('Authority'), ['status' => StatusPaymentEnum::OK->value]);
            DB::commit();
            return $this->responseJson($response->json(), Response::HTTP_CREATED);
        }catch (\Exception $exception){
            DB::rollBack();
            throw new PaymentProblemException();
        }
    }

    public function paymentBodySend(array $request)
    {
        $this->paymentSend['amount'] = $request['amount'];
        $this->paymentSend['description'] = $request['description'];

    }

    public function requestBodySend(array $request)
    {
        $request['gateway'] = GatewayEnum::ZARINPAL->value;
        return $request;
    }

    public function validate(Request $request)
    {
         $request->validate([
             'Authority' => 'required|exists:user_payments,authority',
            'Status' => 'required|in:OK,NOK'
        ]);
    }

    public function createRoute($authority)
    {
        return
            $this->route['routeGateway'] . $authority;
    }
}
