<?php

namespace App\Repository;

use App\Enum\GatewayEnum;
use App\Exceptions\FirstOrFailException;
use App\Models\UserPayments;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class UserPaymentRepository extends BaseRepository
{
    public function __construct(UserPayments $model)
    {
        parent::__construct($model);
    }
    public function findByAuthority($authority)
    {
        try {
            $this->model->query()->where('authority', $authority)
                ->where('status', 'waiting')
                ->firstOrFail();
        }catch (\Exception $exception){
            throw new FirstOrFailException();
        }
    }
    public function findByAuthorityVerify($authority, $mer)
    {
        $res =  $this->model->query()->where('authority', $authority)->first(['amount', 'authority']);
        $res->setAttribute('merchant_id', $mer);
        return $res->toArray();
    }

    public function update($authority, array $data)
    {
        return $this->model->query()->where('authority',$authority)->update($data);
    }

    public function updateWithInsertDetails($authority, array $data, array $details)
    {
            $this->model->query()->where('authority',$authority)->update($data);
                return
                    $this->model->query()->where('authority', $authority)->first()
                        ->paymentDetails()->create($details);
    }

    public function findTopGateway()
    {
        $resQuery = $this->model->query()->whereHas('paymentDetails', function ($query) {
            $query
                ->whereTime('created_at', '>', Carbon::now()->subMinutes(320))
                ->whereTime('created_at', '<', Carbon::now())
                ->where('status', 1);
        })->groupBy('gateway')->select('gateway', DB::raw('count(*) as total'))->get();
        if (count($resQuery) == 0){
             $gatewayList = array_column(GatewayEnum::cases(), 'value');
             $gatewayKey = array_rand($gatewayList);
             return ['gateway' => $gatewayList[$gatewayKey]];
        }else{
            foreach ($resQuery as $res) {
                $gateway[$res['gateway']] = $res['total'];
            }
            arsort($gateway);
            return ['gateway' => array_key_first($gateway)];
        }
    }
}
