<?php

namespace App\Repository;

use App\Exceptions\DbException;
use App\Exceptions\FirstOrFailException;
use Illuminate\Database\Eloquent\Model;

class BaseRepository
{
    public function __construct(public Model $model)
    {
    }

    public function create(array $data)
    {
        return $this->model->query()->create($data);
    }

    public function update($id,array $data)
    {
        return $this->model->query()->where('id', $id)->update($data);
    }

}
