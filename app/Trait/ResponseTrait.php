<?php

namespace App\Trait;


trait ResponseTrait
{
    public function responseJson(mixed $data,int $status)
    {
        return
            response()->json([
                'success' => true,
                'data' => $data,
            ], $status);
    }
}
