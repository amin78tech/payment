<?php

namespace App\Http\Controllers;

use App\Enum\GatewayEnum;
use App\Exceptions\SelectPaymentException;
use App\Http\Requests\PaymentShowValidation;
use App\payment\Payment;
use App\Service\UserPaymentService;
use App\Trait\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    use ResponseTrait;
    public function __construct(public UserPaymentService $paymentService)
    {
        Auth::attempt(['email' => 'amin78tech@gmail.com', 'password' => '9099101010',]);
    }

    public function payment(PaymentShowValidation $request)
    {
        if ($request->get('automatic') == true)
            $request->merge($this->paymentService->findTopGateway());
        $request->merge(['user_id'=> auth()->user()->id]);
        $payment = new Payment(resolve($request->get('gateway')));
        return $payment->run($request->only(['user_id', 'amount', 'description']));
    }

    public function callback(Request $request, $gateway)
    {
        $gateways = array_column(GatewayEnum::cases(), 'value');
        if (in_array($gateway, $gateways) == true){
            $payment = new Payment(resolve($gateway));
            return $payment->callback($request);
        }
        throw new SelectPaymentException();
    }
}
