<?php

namespace App\Http\Requests;

use App\Enum\GatewayEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class PaymentShowValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'gateway' => ['required_if:automatic,0',
                new Enum(GatewayEnum::class),
                ],
            'amount' => 'required|integer',
            'description' => 'required',
            'automatic' => 'sometimes|boolean'
        ];
    }
}
