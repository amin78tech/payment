<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class PaymentProblemException extends Exception
{
    public function render($request)
    {
        return response()->json(["message" => 'درگاه پرداخت به مشکل خورده است بعدا تلاش نمایید',
            'error' => [
                'gateway' => 'درگاه پرداخت به مشکل خورده است بعدا تلاش نمایید'
            ]
            ],Response::HTTP_BAD_GATEWAY);
    }
}
