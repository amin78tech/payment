<?php

namespace App\Exceptions;

use Exception;

class CancelOrFailPaymentException extends Exception
{
    public function render($request)
    {
        return response()->json([ "message" => 'پرداخت با خطا مواحه شد یا توسط شما کنسل شد',
        "error" => [
            'payment' => 'پرداخت با خطا مواحه شد یا توسط شما کنسل شد'
        ],
        ],422);
    }
}
