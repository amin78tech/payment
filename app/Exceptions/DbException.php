<?php

namespace App\Exceptions;

use Exception;

class DbException extends Exception
{
    public function render($request)
    {
        return response()->json(['message' => 'خطا در دیتابیس',
                "error" => [
                    'payment' => 'خطا در دیتابیس'
                ],]
            ,404);
    }
}
