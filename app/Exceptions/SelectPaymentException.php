<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class SelectPaymentException extends Exception
{
    public function render($request)
    {
        return response()->json(["message" => 'پارمتر ورودی اشتباه است',
            'error' => [
                'gateway' => 'پارمتر ورودی اشباه است'
            ]
        ],422);
    }
}
