<?php

namespace App\Exceptions;

use Exception;

class FirstOrFailException extends Exception
{
    public function render($request)
    {
        return response()->json(['message' => 'نامعتبر به علت وضعیت یا کد وارد شده',
                "error" => [
                    'payment' => 'نامعتبر به علت وضعیت یا کد وارد شده'
                ],]
            ,404);
    }
}
