<?php

namespace App\Enum;

enum GatewayEnum :string
{
    case ZARINPAL = 'zarinPal';
    case ZIBAL = 'zibal';
}
