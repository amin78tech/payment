<?php

namespace App\Enum;

enum StatusPaymentEnum : string
{
    case OK = 'ok';
    case NOK = 'nok';
    case WAITING = 'waiting';

}
